from telebot.types import \
        ReplyKeyboardMarkup,\
        InlineKeyboardMarkup,\
        KeyboardButton,\
        InlineKeyboardButton


class Keyboard:
    def __init__(self):
        self.btns = Buttons()

    @property
    def start(self):
        kb = ReplyKeyboardMarkup()
        kb.add(self.btns.search)
        return kb

    def response(self, url, text):
        kb = InlineKeyboardMarkup()
        kb.add(self.btns.response_url(url, text))
        return kb


class Buttons:
    @property
    def search(self):
        return KeyboardButton('🔍Поиск🔍🔍🔍🔍🔍')

    def response_url(self, url, text):
        btn = InlineKeyboardButton(text, url=url)
        return btn