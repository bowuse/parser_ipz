from telegram_bot.handlers.handler import Handler
from time import sleep

start_message = 'Приветствую, я помогу тебе с поиском статей на ru.wikipedia\
                                         \nВоспользуйся командой search в формате - /search [запрос]'

class CommandHandler(Handler):
    def activate(self):
        self.add_handler(self.help, commands=['help'])
        self.add_handler(self.start, commands=['start'])
        self.add_handler(self.search, commands=['search'])

    # /help
    def help(self, m):
        self.bot.send_message(m.chat.id, start_message)

    # /start
    def start(self, m):
        self.bot.send_message(m.chat.id, start_message)

    # /search
    def search(self, m):
        request_text = m.text.replace('/search', '').strip()
        if not request_text:
            self.bot.send_message(m.chat.id, 'Не хорошо отправлять пустой запрос...')
        else:
            response = self.searcher.find_bm25(request_text, self.search_index)
            if not response:
                self.bot.send_message(m.chat.id, 'Ничего не найдено')
            response = list(response)
            if len(response) > 15:
                response = response[-15:]
            for result in response:
                self.bot.send_message(m.chat.id,
                                      text='✔',
                                      reply_markup=self.kb.response(
                                          url=result[1],
                                          text=result[2]
                                      ))
                sleep(0.5)

