import telebot
from telegram_bot.env import TOKEN
from telegram_bot.handlers.commands import CommandHandler
from telegram_bot.handlers.types import TypesHandler
from telegram_bot.handlers.text import TextMessageHandler
from telegram_bot.handlers.calls import CallBackHandler

bot = telebot.TeleBot(TOKEN)

CommandHandler(bot).activate()
TypesHandler(bot).activate()
TextMessageHandler(bot).activate()
CallBackHandler(bot).activate()

if __name__ == '__main__':
	print('Bot is started & running')
	print(bot.get_me())
	bot.polling()