# -*- coding: utf-8 -*-
import scrapy
import re
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class WikiSpider(CrawlSpider):
    name = 'wiki'
    allowed_domains = ['ru.wikipedia.org']
    start_urls = ['https://ru.wikipedia.org/']

    counter = 0

    custom_settings = {
        'DOWNLOAD_DELAY': 1,
    }

    rules = [
        Rule(
            LinkExtractor(
                restrict_xpaths=['//div[@id="mw-content-text"]'],
                deny=('index.php', 'Шаблон')
                ),
            follow=True,
            callback='parse_page'),
    ]

    def parse_page(self, response):
        item = {}

        item['id'] = self.counter
        item['url'] = response.url
        item['name'] = response.xpath('//h1[@id="firstHeading"]/text()').get()

        article_list = response.xpath('//div[@id="mw-content-text"]//p//text()').extract()
        clean_article_list = [re.sub('\[.*\]', '', i).strip() for i in article_list]
        clean_article = ' '.join(filter(lambda x: len(x) > 1, clean_article_list))
        item['article'] = clean_article

        self.counter += 1
        return item

