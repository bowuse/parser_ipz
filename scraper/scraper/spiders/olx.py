# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class OlxSpider(CrawlSpider):
    name = 'olx'
    allowed_domains = ['olx.ua']
    start_urls = ['http://olx.ua/']

    id_counter = 0

    rules = [
        # categories
        Rule(
            LinkExtractor(restrict_xpaths=('//div[@class="maincategories"]'))
        ),
        # item
        Rule(
            LinkExtractor(restrict_xpaths=('//div[@class="offer-wrapper"]'), allow='/obyavlenie/'),
            callback='parse_item'
        ),
        # pagination
        Rule(
            LinkExtractor(restrict_xpaths=('//div[@class="pager rel clr"]'), allow='page=')
        )
    ]

    def parse_item(self, response):
        item = {}
        item['url'] = response.url
        item['id'] = self.id_counter
        self.id_counter += 1
        # item['price'] = response.xpath('//div[@class="price-label"]/strong/text()').extract()
        # item['img_urls'] = response.xpath('//div[@class="tcenter img-item"]//img/@src').extract()

        info = []
        info += response.xpath('//meta[@name="description"]/@content').extract()
        info += response.xpath('//title/text()').extract()
        info += [i.strip() for i in response.xpath('//div[@class="offer-titlebox__details"]//text()').extract() if i.strip()]
        item['info'] = info[0]

        yield item