# Parser_IPZ

to install requirement libraries via pip use:

`pip install -r requirements.txt`

## Run spider

First you need to parse data from wikipedia:

`cd scraper`

`scrapy crawl wiki -o wiki.csv -t csv`

`cd ..`

Saves scraped items to wiki.csv

## Make index

Second, index data 

`python3 searcher/file_indexer.py`

It will save indexed data in searcher/wiki_index.json

## Run telegram bot

Create env.py and save there telegram bot token

`TOKEN='your_token_here'`

Finally, you can run bot with:

`python3 telegram_bot/app.py`

