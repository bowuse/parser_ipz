import csv
import json
import re
from collections import namedtuple
from stemmer import Stemmer
from time import time

class FileIndexer:
	def __init__(self, input_filenames, output_filename=''):
		self.filenames = input_filenames
		self.output_filename = output_filename

	def file_termlist(self):
		termlist = {}
		for filename in self.filenames:
			fileformat = filename.split('.')[1]
			if fileformat == 'json':
				termlist.update(self.process_jsonfile(filename))
			elif fileformat == 'csv':
				termlist.update(self.process_csvfile(filename))
			else:
				termlist.update(self.process_file(filename))
		return termlist

	def process_jsonfile(self, filename):
		file_to_termes = {}
		with open(filename, 'r') as f:
			filedata = json.load(f)
		PageData = namedtuple('PageData', ('url', 'name', 'article'))
		for page in filedata:
			article = FileIndexer.process_query(page['article'])
			name = [word.lower() for word in page['name'].split()]
			file_to_termes[page['id']] = PageData(page['url'], name, article)

		return file_to_termes

	def process_csvfile(self, filename):
		file_to_termes = {}
		PageData = namedtuple('PageData', ('url', 'name', 'article'))
		csv.field_size_limit(500000)
		with open(filename, 'r') as file:
			r = csv.reader(file)
			for page in r:
				page_id = page[0]
				article = FileIndexer.process_query(page[3])
				name = [word.lower() for word in page[2].split()]
				url = page[1]
				file_to_termes[page_id] = PageData(url, name, article)

		return file_to_termes

	def process_file(self, filename):
		files_to_termes = {}
		with open(filename, 'r') as f:
			files_to_termes[filename] = ''
			for line in f:
				files_to_termes[filename] += line
		files_to_termes[filename] = FileIndexer.process_query(files_to_termes[filename])
		return files_to_termes

	# static
	def process_word(word):
		cword = word.lower().strip()
		return Stemmer().stem(cword)

	# static
	def process_query(query):
		cquery = re.sub(r'[\W_]+', ' ', query)
		return list(map(FileIndexer.process_word, cquery.split(' ')))

	def index_page_data(self, page_data):
		page_name = map(FileIndexer.process_word, page_data.name)
		page_index = {word: [-1] for word in page_name}
		for index, word in enumerate(page_data.article):
			if word in page_index:
				page_index[word].append(index)
			else:
				page_index[word] = [index]
		return page_index

	def make_indices(self, termdict):
		indexed_words = {}
		for _id in termdict:
			indexed_words[_id] = self.index_page_data(termdict[_id])
		return indexed_words

	def full_index(self, indexed_words):
		total = {'page_name':{}}
		for _id in indexed_words:
			for word in indexed_words[_id]:
				if word in total:
					total[word][_id] = indexed_words[_id][word]
				else:
					total[word] = {_id: indexed_words[_id][word]}
		return total

	def save_to_file(self):
		terms = self.file_termlist()
		indexed_words = self.make_indices(terms)
		index = self.full_index(indexed_words)
		index['bm25_parameters'] = {
			'D': {file: len(words) for file, words in terms.items()},					# doc lenght
			'avgdl': sum([len(value) for key, value in terms.items()]) / len(terms) 	# average doc length
		}
		with open(self.output_filename, 'w') as file:
			json.dump(index, file)


def main():
	infile = ['scraper/wiki.csv']
	outfile = 'searcher/wiki_index.json'
	indexer = FileIndexer(infile, outfile)
	indexer.save_to_file()

if __name__ == '__main__':
	start_time = time()
	main()
	print("--- {0:.3f} seconds ---".format(time() - start_time))
