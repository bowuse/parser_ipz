import json
import csv
from math import log10
from searcher.file_indexer import FileIndexer


class SearchEngine:
	'''
		Simple search engine used to find most relevant files to request
		Uses bm25 method
	'''

	def __init__(self, raw_file, indexed_file):
		self.indexed_file = indexed_file
		self.raw_file = raw_file

	def full_index(self):
		with open(self.indexed_file, 'r') as file:
			data = json.loads(file.read())
		return data

	def one_word_query(self, word, full_index):
		if word in full_index:
			return [file for file in full_index[word]]
		return []

	def files_contain_words(self, words, full_index):
		"""
			Files containing all words from query
		"""
		res = self.one_word_query(words[0], full_index)
		for i in range(1, len(words)):
			res = list(set(self.one_word_query(words[i], full_index)) & set(res))
		return res

	def exact_words(self, words, full_index):
		"""
			method return amount of words that file contains
			in same order as in query_words
		"""
		res = {}
		for file in self.files_contain_words(words, full_index):
			res[file] = []
			for word in words:
				res[file] += full_index[word][file]
			res[file].sort()

		score = {}
		for file in res:
			score[file] = [1]
			count = 1
			for i in range(len(res[file])-1):
				if res[file][i] - res[file][i+1] == -1:
					count += 1
				else:
					score[file].append(count)
					count = 1
			score[file] = max(score[file])
		return score

	def count_tf(self, D, q):
		q = len(q)
		return q / D

	def count_idf(self, D, d_i):
		D, d_i = len(D), len(d_i)
		#return log10((D - d_i + 0.5)/ (d_i + 0.5))
		return log10(D / d_i)


	def bm25(self, query, full_index):
		# https://habr.com/ru/post/162937/
		score = {}
		query_words = FileIndexer.process_query(query)
		exact_query = self.exact_words(query_words, full_index)
		D = full_index['bm25_parameters']['D']
		avgdl = full_index['bm25_parameters']['avgdl']
		k = 3.4
		b = 0.3
		for word in query_words:
			if word in full_index:
				for file in full_index[word]:
					if file not in score:
						score[file] = 0
					words_file = full_index[word][file]
					idf = self.count_idf(D, d_i=words_file)
					tf = self.count_tf(D[file], q=words_file)
					file_d = D[file]
					score[file] += idf * tf * (k+1) / (tf + k * (1 - b + b * file_d / avgdl))

					# find article name
					if -1 in words_file:
						score[file] += 2*k
			else:
				return
		for file in score:
			if file in exact_query:
				score[file] *= exact_query[file]
		return sorted(score.items(), key=lambda x: x[1])

	def find_bm25(self, query, full_index):
		search_result = self.bm25(query, full_index)
		if not search_result:
			return
		with open(self.raw_file[0], 'r') as file:
			csv.field_size_limit(5000000)
			search_data = []
			for r in csv.reader(file):
				for page in search_result:
					if r[0] == page[0]:
						search_data.append(r)
		while search_result:
			target = search_result.pop(0)
			for data in search_data:
				if data[0] == target[0]:
					yield data[:-1]
